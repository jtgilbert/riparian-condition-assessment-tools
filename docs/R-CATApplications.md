---
title: R-CAT Applications
---

[![RCAT_Banner_Web](assets/images/RCAT_Banner_Web.png)]({{ site.url }})


[R-CAT: Riparian Condition Assessment Tool]({{ site.url }})‎ >

### R-CAT Applications

As we complete deliverable applications of R-CAT, we post them in the following sub-pages:

#### R-CAT Applications

- [Columbia River Basin (CRB) Implementation]({{ site.baseurl }}/CRBImplementation)
  - [CRB R-CAT Map Atlas]({{ site.baseurl }}/CRBR-CATMapAtlas)
- [Utah Implementation]({{ site.baseurl }}/UtahImplementation)
  - [LANDFIRE Data Limitations]({{ site.baseurl }}/Landfire)
  - [Utah R-CAT Map Atlas]({{ site.baseurl }}/UtahR-CATMapAtlas)
  - [Weber River Styles Comparison]({{ site.baseurl }}/WeberRiverStylesComparison)